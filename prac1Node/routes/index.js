/*
  /routes/index.js
*/
const express = require('express');
const router = express.Router();
const path = require('path');
var InputController = require('../controllers/user.controller.js');



//Middleware para mostrar datos del request
router.use (function (req,res,next) {
  console.log('/' + req.method);
  next();
});

//Analiza la ruta y el protocolo y reacciona en consecuencia
router.get('/',function(req,res){
  res.sendFile(path.resolve('views/form.html'));
});

router.post('/userController',InputController.isMayor);
router.post('/userController',InputController.saveUser);


router.get('/listdb',InputController.list);

module.exports = router;