var User = require('../models/user.model.js');

exports.isMayor = (req, res, next) => {
    let birthDateReq = req.body.age;

    let birthDate = new Date (birthDateReq);
    let todayDate = new Date (Date.now());

    let difference = todayDate.getFullYear() - birthDate.getFullYear() - 1;

    let isOldEnough = false;
    if (difference >= 18) {
        isOldEnough = true;
    }
    req.isOldEnough = isOldEnough;
    next();
}

exports.saveUser = (req, res, next) => { 
    let userNameReq = req.body.name;
    let birthDateReq = req.body.age;

    let user = new User({name: userNameReq, birthDate: birthDateReq, above18: req.isOldEnough});
    user.save();
    res.send(user);
}

exports.list = (req, res) => {
    User.find(function(err, users) {
        res.send(users);
    });
};