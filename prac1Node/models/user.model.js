const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var userSchema = new Schema({
    name: String,
    birthDate: Date,
    above18: Boolean
});

const User = mongoose.model('User', userSchema);
module.exports = User;