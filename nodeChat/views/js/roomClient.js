include 
var socket = io();

var messages = document.getElementById('messages');
var form = document.getElementById('form');
var input = document.getElementById('text');

//banner
var banner = document.createElement('p');

var username = finUserName;
banner.textContent = "Welcome to chat room " + username;
messages.appendChild(banner);

form.addEventListener('submit', function(e) {
e.preventDefault();
if (input.value) {
    socket.emit('chat message', input.value);
    input.value = '';
}
});

socket.on('chat message', function(msg) {
    var item = document.createElement('p');
    item.textContent = msg;
    messages.appendChild(item);
    window.scrollTo(0, document.body.scrollHeight);
});