var User = require('../models/user.model.js');

exports.saveUser = (req, res, next) => { 
    let user = new User({username: req.body.username, password: req.body.password});
    user.save();
    res.redirect("/")
}

exports.login = (req, res, next) => { 
    User.findOne({ username: req.body.username, password: req.body.password }, function (err, user) {
        if (user != undefined) {
            var sess = req.session;
            sess.userid = user._id;
            sess.username = user.username;
            console.log(req.session.username);
            //res.render(req.session.userid, req.session.username)
            next();
        } else {
            console.log("Incorrect login");
            res.redirect("/")
        }
    });
}
