var express = require('express');
var app = express();
var mongoose = require('mongoose');
const index = require('./routes/index.js');
const path = __dirname + '/views/';

const Path = require('path');

app.set('view engine', 'pug');
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path));

app.use('/css', express.static(Path.join(__dirname, 'node_modules/bootstrap/dist/css')));
app.use('/js', express.static(Path.join(__dirname, 'node_modules/bootstrap/dist/js')));


app.use('/', index);

mongoose.connect(
  `mongodb://root:pass12345@localhost:27017/tutorial?authSource=admin`,
  { useUnifiedTopology: true, useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
  }
);

//socket part
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;

//to save thge list of users in all rooms simultaneously
listOfConnecteUsersInsideRooms = new Array();
for (let index = 1; index <= 4; index++) {
  let room = {
    roomnum: index,
    listUsers: new Array()
  }
  room.roomnum = index;
  listOfConnecteUsersInsideRooms.push(room);
}

function addUserList(roomNum, username) {
  for (let index = 0; index < 4; index++) {
    if (roomNum == listOfConnecteUsersInsideRooms[index].roomnum) {
      listOfConnecteUsersInsideRooms[index].listUsers.push(username);
    }
  }
}

function getUserList(roomNum) {
  for (let index = 0; index < 4; index++) {
    if (roomNum == listOfConnecteUsersInsideRooms[index].roomnum){
      return listOfConnecteUsersInsideRooms[index].listUsers;
    }
  }
}

io.on('connection', (socket) => {
  socket.on('room1', msg => {
    io.emit('room1', msg);
  });
});

io.on('connection', (socket) => {
  socket.on('room2', msg => {
    io.emit('room2', msg);
  });
});

io.on('connection', (socket) => {
  socket.on('room3', msg => {
    io.emit('room3', msg);
  });
});

io.on('connection', (socket) => {
  socket.on('room4', msg => {
    io.emit('room4', msg);
  });
});

io.on('connection', (socket) => {
  socket.on('roomlist1', msg => {
    addUserList(1,msg);
    io.emit('roomlist1', getUserList(1));
  });
});

io.on('connection', (socket) => {
  socket.on('roomlist2', msg => {
    addUserList(2,msg);
    io.emit('roomlist2', getUserList(2));
  });
});

io.on('connection', (socket) => {
  socket.on('roomlist3', msg => {
    addUserList(3,msg);
    io.emit('roomlist3', getUserList(3));
  });
});

io.on('connection', (socket) => {
  socket.on('roomlist4', msg => {
    addUserList(4,msg);
    io.emit('roomlist4', getUserList(4));
  });
});



http.listen(port, () => {
  console.log(`Socket.IO server running at http://localhost:${port}/`);
});


