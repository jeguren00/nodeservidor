const mongoose = require('mongoose');
const User = require('./user.model');
const Schema = mongoose.Schema;

var message = new Schema({
    author: User,
    content: Text
});

const Message = mongoose.model('Message', message);
module.exports = Message;
