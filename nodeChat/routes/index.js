/*
  /routes/index.js
*/
const express = require('express');
const app = express();

const router = express.Router();
const path = require('path');
var userController = require('../controllers/user.controller.js');
var roomController = require('../controllers/room.controller.js');

const session = require('express-session');

connectedUsers = Array();

//Middleware para mostrar datos del request
router.use (function (req,res,next) {
  console.log('/' + req.method);
  next();
});

router.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false}
}));

//Analiza la ruta y el protocolo y reacciona en consecuencia
router.get('/',function(req,res){
  res.sendFile(path.resolve('views/start.html'));
});

router.get('/user/register',function(req,res){
  res.sendFile(path.resolve('views/register.html'));
});
router.post('/user/save',userController.saveUser);

//login
router.post('/user/login',userController.login);
router.post('/user/login',function(req,res){
  res.redirect("/roomSelect");
});

router.get('/roomSelect',function(req,res){
  res.sendFile(path.resolve('views/roomSelect.html'));
});

router.post('/toRoom',function(req,res){
  //dosent work, old way still used
  connectedUsers[req.sessionID] = req.session.username;
  res.render('room.pug', {username: req.session.username, roomNumber: req.body.room, connectedUsers: connectedUsers  })
  //res.sendFile(path.resolve('views/room.html'));
});


/*router.get('/test', (req, res) => {
  console.log(req.session);
  console.log(req.session.username);
  res.render('test.pug', {message: req.session.username })
})*/


module.exports = router;
